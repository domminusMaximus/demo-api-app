CREATE TABLE IF NOT EXISTS `person` (
    `id` int(10) NOT NULL auto_increment,
    `name` varchar(255),
    `surname` varchar(255),
    `email` varchar(255),
    `phone` varchar(255),
    `date_of_birth` date,
    `age` numeric(9,2),
    `username` varchar(255),
    `password` varchar(255),
    PRIMARY KEY( `id` )
    );