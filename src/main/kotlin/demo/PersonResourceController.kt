package demo

import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/persons")
class PersonResourceController(val service: PersonService) {

    @GetMapping
    fun getAll(@RequestParam page : Int): ResponseEntity<Any> {
        //Hard coded and low value for simplicity
        val perPage = 2
        val findAll = service.findPersons(PageRequest.of(page -1, perPage))
        if (findAll.isEmpty()) {
            return ResponseEntity<Any>(HttpStatus.NO_CONTENT)
        }
        val total = service.db.count()
        val pagination = PaginatedResponse(persons = findAll, total.toInt(), page, (total.toInt()/perPage) + 1)
        return ResponseEntity<Any>(pagination, HttpStatus.OK)
    }

    @GetMapping("/filterByAge")
    fun getAllByAge(@RequestParam minAge: Int, @RequestParam maxAge: Int): ResponseEntity<List<Person>> {
        val findByAge = service.findPersonsByAge(minAge, maxAge)
        if (findByAge.isEmpty()) {
            return ResponseEntity<List<Person>>(HttpStatus.NO_CONTENT)
        }
        return ResponseEntity<List<Person>>(findByAge, HttpStatus.OK)
    }


    @GetMapping("/filterByName")
    fun getAllByName(@RequestParam name:String): ResponseEntity<List<Person>> {
        val findByName = service.findPersonsByName(name)
        if (findByName.isEmpty()) {
            return ResponseEntity<List<Person>>(HttpStatus.NO_CONTENT)
        }
        return ResponseEntity<List<Person>>(findByName, HttpStatus.OK)
    }

    @GetMapping("/{id}")
    fun findPersonById(@PathVariable id: Long): ResponseEntity<Person> {
        val person = service.findPerson(id)
        if (person.isPresent) {
            return ResponseEntity<Person>(person.get(), HttpStatus.OK)
        }
        return ResponseEntity<Person>(HttpStatus.NO_CONTENT)
    }

    @PutMapping("/{id}")
    fun updatePerson(@PathVariable("id") id: Long, @RequestBody person: Person): ResponseEntity<Person>? {
        val persistedPerson = service.findPerson(id)

        if (persistedPerson.isEmpty) {
            return ResponseEntity<Person>(HttpStatus.NO_CONTENT)
        }
        return persistedPerson.map { personDetails ->
            val updatedPerson: Person = personDetails.copy(
                name = person.name,
                surname = person.name,
                email = person.email,
                phone = person.phone,
                dateOfBirth = person.dateOfBirth,
                age = person.age,
                password = person.password,
                username = person.username
            )
            ResponseEntity(service.db.save(updatedPerson), HttpStatus.OK)
        }.orElse(ResponseEntity<Person>(HttpStatus.INTERNAL_SERVER_ERROR))
    }

    @PostMapping
    fun addPerson(@RequestBody person: Person) : ResponseEntity<Person> {
        service.db.save(person)
        return ResponseEntity<Person>(HttpStatus.CREATED)
    }

    @DeleteMapping("/{id}")
    fun deletePerson(@PathVariable id:Long)  {
        service.deletePerson(id)
    }
}