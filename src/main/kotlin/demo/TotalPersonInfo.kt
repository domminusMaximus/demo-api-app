package demo

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.actuate.endpoint.annotation.Endpoint
import org.springframework.boot.actuate.info.Info
import org.springframework.boot.actuate.info.InfoContributor
import org.springframework.stereotype.Component

/**
 * This is bespoke actuator that will display number of persons in DB. It is accessible with /info.
 */
@Component
class TotalPersonInfo(val personService : PersonService) : InfoContributor {

    override fun contribute(builder: Info.Builder?) {
        var totalPerson = personService.db.count();
        builder?.withDetail("Total number of persons:", totalPerson)
    }
}