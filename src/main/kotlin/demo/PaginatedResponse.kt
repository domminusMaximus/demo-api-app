package demo

class PaginatedResponse(
    val persons: List<Person>,
    val total: Int,
    val page: Int,
    val last_page: Int
)
