package demo

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder

@Configuration
class SecurityConfig : WebSecurityConfigurerAdapter() {

    @Bean
    fun encoder(): PasswordEncoder {
        return BCryptPasswordEncoder()
    }


    override fun configure(auth: AuthenticationManagerBuilder) {
        auth.inMemoryAuthentication()
            .withUser("admin")
            .password(encoder().encode("password"))
            .roles("ADMIN")
            .and()
            .withUser("guest")
            .password(encoder().encode("password"))
            .roles("GUEST")
    }

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        http.httpBasic()
            .and()
            .authorizeRequests()
            .antMatchers(HttpMethod.POST, "/api/persons/**").hasRole("ADMIN")
            .antMatchers(HttpMethod.PUT, "/api/persons/**").hasRole("ADMIN")
            .antMatchers(HttpMethod.DELETE, "/api/persons/**").hasRole("ADMIN")
            .antMatchers(HttpMethod.GET, "/api/persons/filterByAge").hasAnyRole("ADMIN", "GUEST")
            .antMatchers(HttpMethod.GET, "/api/persons/filterByName").hasAnyRole("ADMIN", "GUEST")
            .antMatchers(HttpMethod.GET, "/api/persons").hasRole("ADMIN")
            .and()
            .csrf().disable().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
            .formLogin().disable()
    }

}