package demo

import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import java.awt.print.Pageable
import java.util.*

@Service
class PersonService(val db: PersonRepository) {

    fun findPersons(pageable: PageRequest): List<Person> {
        return db.findAll(pageable).iterator().asSequence().toList()
    }

    fun findPersonsByAge(minAge: Int, maxAge: Int) = db.getPersonByAge(minAge, maxAge)

    fun findPersonsByName(name: String) = db.getPersonByAge(name)

    fun deletePerson(id: Long) = db.deleteById(id)

    fun findPerson(id: Long): Optional<Person> = db.findById(id)

    fun save(person: Person) {
        db.save(person)
    }
}