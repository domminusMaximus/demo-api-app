package demo

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table
import java.util.Date

@Table("PERSON")
data class Person(
    @Id val id: Long?,
    val name: String,
    val surname: String,
    val email: String,
    val phone: String,
    val dateOfBirth: Date,
    val age: Int,
    val username: String?,
    val password: String?,
)
