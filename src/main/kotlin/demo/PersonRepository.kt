package demo

import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.PagingAndSortingRepository

interface PersonRepository : PagingAndSortingRepository<Person, Long> {

    @Query("select name, surname, email, phone, date_of_birth, age from person where age between :minAge and :maxAge")
    fun getPersonByAge(minAge:Int, maxAge:Int): List<Person>

    @Query("select name, surname, email, phone, date_of_birth, age from person where name = :name")
    fun getPersonByAge(name: String): List<Person>
}