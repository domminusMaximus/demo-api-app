package demo

import com.google.gson.Gson
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

/**
 * Custom response for application exception -
 * there could more based on the different types of exceptions, including customs.
 */
@ControllerAdvice
class APIExceptionHandler : ResponseEntityExceptionHandler() {
    @ExceptionHandler(value = [(Exception::class)])
    fun bespokeHandleException(exception: Exception, request: WebRequest): ResponseEntity<Any> {
        var gson = Gson()
        var jsonString = gson.toJson(ErrorJson(HttpStatus.INTERNAL_SERVER_ERROR.name,
            "Something went wrong", exception.message))
        return handleExceptionInternal(exception, jsonString, HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request)
    }

    class ErrorJson(
        val error: String,
        val message: String,
        val detailMessage: String?,
    )
}