package demo

import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.util.AssertionErrors.assertEquals
import org.springframework.test.util.AssertionErrors.assertNotNull


@SpringBootTest
class DemoApplicationTests {

	@InjectMocks
	lateinit var personResourceController: PersonResourceController
	@Mock
	lateinit var personService: PersonService

	@Test
	fun testNoPersonFoundById() {
		val result = personResourceController.findPersonById(100000)
		assertEquals("Find person by id found nothing.",HttpStatus.NO_CONTENT, result.statusCode)
	}

	@Test
	fun testFindAll() {
		val result = personResourceController.getAll()
		assertEquals("Get all returned nothing.",HttpStatus.NO_CONTENT, result.statusCode)
	}

}
