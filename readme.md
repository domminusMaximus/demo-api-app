# API Demo App

This is API app demo.

## Installation

Please clone it from the public repository.

## Usage

API docs can be accessed via http://localhost:8080/swagger-ui/#/person-resource-controller
once the application is running on localhost.
This is just automated based but should be enough for that API.

Additionally, there is also request.http file that can be used inside project if EDI supports it.

Extra actuator that tells what number of rows can be access via /actuators/info

API request are guarded by spring security that have two roles ADMIN and GUEST
usernames: admin, guest 
passwords: password 

